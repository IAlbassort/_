# What is this?
This was a minecraft-fabric interopt test. The idea was to create code in Nim which would manage high physics calculations which would be implemented. The calculations would be sent over to Java at a polling rate; resulting in an internal server which was nim, running in a java shell. But this proved to be too complicated due to weird FFI function calling. This idea might be revisited by me at another time :)

This was a basic ss13 chem proof of concept implementation; which came along with it.

The scope of this project proved to be too insane, because minecraft modding is kinda like fighting the flow of a tsunami. I lack all of the control I would need in order to make production even the tiniest bit of efficient. Doing this stuff alone without a team is pretty impossible, and so I quit while i was ahead.