package net.albassort.station
import net.albassort.station.libclass.*
import java.net.*
import java.io.*
import java.nio.channels.*
import java.util.concurrent.*
import java.nio.charset.StandardCharsets
//NIM INTEROP TEST
//This was a way to interface with the nim language server.
class mainclass{
companion object{      
        fun main(){
            libinit.extractlibraries()
            Thread{server()}.start()
            val helloworld = libclass.libhelloworld()
            Thread{helloworld.helloworld()}.start()
            Thread{sendnim("test")}.start()
         }   
    }
}
fun server() {
    val server = ServerSocket(2021, 1, InetAddress.getByName("127.0.0.1"))
    printx("Sever Starting!")
    val sleepytime : MutableList<String> = ArrayList()
    while(true){
        val client = server.accept()
        val input = BufferedReader(InputStreamReader(client.inputStream))
        val actualinput = input.readLine()
        if (actualinput.take(4) == "test"){
            for (i in actualinput.split("@[")[1].split("]")[0].split(",")){
                sleepytime.add(i)
            }
        }
        printx(sleepytime)
        System.exit(0)

    }
}
fun printx(string : Any){
    println("Kotlin: "+string)
}
fun sendnim(s : String){
    try{
    val client = Socket("127.0.0.1", 2022)
    client.outputStream.write("${s}\r\\L".toByteArray())
    client.close()} 
    catch (e: Exception)
    {printx("Trying again!")
    Thread.sleep(1000)
    sendnim("Retry")}
    }
