package net.albassort.station
import jnr.ffi.LibraryLoader
import net.albassort.station.gettempdir
import net.albassort.station.libmodify
class libclass{
    companion object {
       fun libhelloworld() : helloworld{
          return LibraryLoader.create(helloworld::class.java).load("${gettempdir()}/${libmodify("helloworld")}")}
    }
   public interface helloworld{
       fun sendtojava(s : String)
       fun print(s  : String)
       fun helloworld()
   }
}
