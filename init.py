from PIL import Image
import os
from os import path

#Created around March 2021 for a seperate project.
#useful io functions, only 3 are needed.

def writemakepath(path):
    if os.path.exists(path) == False:
        makepath = path.rpartition("/")
        #replace vulnerable os.system()...
        #if the file is in the dir, it will make it
        if not makepath[0]:
            with open(path, 'w') as temp:
                temp.close
        else:
            #if not, it will make the folder.
            if os.path.exists(makepath[0]) == False:
                os.makedirs(makepath[0])
            with open(f'{makepath[0]}/{makepath[2]}', 'w') as temp:
                temp.close

def writelist(listx, file):
    writemakepath(file)
    with open(file, 'a') as f:
        for xy in listx:
            f.writelines(str(xy)+"\n")
        f.close

def readall(file):
    #...reads file
    try:
        fileopen = open(file).readlines()
        returnlist = []
        for x in fileopen:
            returnlist.append(x.rstrip())
        return returnlist
    except IOError:
        return "File or line doesn't exist"