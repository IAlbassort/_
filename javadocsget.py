from typing import Tuple
import zipfile
import urllib.request
import os
import shutil
from init import *
#Makeshift html parser
#just a if ladder that passes throughs chars

#Made by retkid, 20th of August, 2021... Caroline.
def dateandtitlegetter(url):
    fid = urllib.request.urlopen(url)
    webpage = fid.read().decode('utf-8')
    webpage = webpage.split("\n")

    readall = False
    templist = []
    templist1 = []
    masterlist = []
    counter = 0
    add1 = False
    counter = 0
    for i in webpage:
        for char in i:
            counter+= 1
            #checks if we're in the date area
            if char == ' ' and i[counter+1].isdigit():
                readall = True
                continue
            if readall:
                #whence we've found the date area we don't let go

                #detects enf odate area
                if char == " " and i[counter+1] == " ":
                    readall = False
                    continue
                #segemnts date area here so we don't have to do it later
                if char == "-" or len(templist) == 4:
                    if len(templist) == 4:
                        #for the time stuff
                        if ":" in templist:
                            templist.append(char)
                        else:
                            add1 = True
                    if not templist:
                        continue
                    #adds everything to master list and clears
                    templist1.append(templist)
                    templist = []
                    if add1:
                        add1 = False
                        templist.append(char)
                    continue
                templist.append(char)


        counter = 0
        readall = False
        templist1.insert(0, (i.partition('ref="')[2]).partition('"')[0])
        masterlist.append(templist1)
        templist1 = []
    return masterlist
month = {"Jan" : 1, "Feb" : 2, "Mar": 3, "Apr": 4, "May":5, "Jun":6, "Jul":7,
        "Aug" : 8, "Sep" : 9, "Oct" : 10, "Nov" : 11, "Dec" : 12}
def sortdir(masterlist : list, url : str, version):
    templist = []
    finallist = []
    #prelimitary checks for addition to the finallist, sorts out the rift-raft
    for i in masterlist:
        if not i or i == [""] or i == ['../']:
            continue
        for x in i:
            templist.append("".join(x))
        if len(templist) == 1 or not templist[0][0].isdigit():
            templist = []
            continue
        if version != "unused":
            if not templist[0].partition(version)[1]:
                templist = []
                continue
        #if x[1] == the date of the month. we just add it
        if templist[1].isdigit() and len(templist[1]) == 2:
            finallist.append(templist)
        else:
            #else we find it with this weird thing
            temptemplist = [templist[0]]
            it = 0
            for i in templist:
                if i.isdigit() and len(i) == 2:
                    temptemplist.append(templist[it])
                    temptemplist.append(templist[it+1])
                    temptemplist.append(templist[it+2] )
                    temptemplist.append(templist[it+3])
                    temptemplist = []
                else:
                    continue
            finallist.append(temptemplist)
            break
        templist = []
    if finallist[-1] == []:
        finallist.pop(-1)
    #pops cuz -1 = [] for some reason
    #sorts the list based on date
    finallist.sort(key=lambda x: (int(x[3]), int(month[x[2]]), int(x[1])))

    ##print(finallist[-1])
   ## print(finallist[1])
    dirx = finallist[-1][0]
    url=url+dirx
    return url


def oscheckexists(givenfile, version = "unused"):
    if os.path.exists(givenfile):
        #does a check if  bool is true, if true checks jar
        if version != "unused ":
            #this string is mangeled dunno why :|
            versionread = readall(f"{givenfile}version.txt")[0].rsplit()[0]
            if version[1] == versionread:
                exit("VERSION MATCH")
        print(f"WARNING {givenfile} EXISTS! Do you want to remove it?")
        x = input("remove? [N/y]\n")
        if x in ["yes", "y", "Y", "YES"]:
            if os.path.isdir(givenfile):
                shutil.rmtree(givenfile)
            else:
                os.remove(givenfile)
        else:
            exit()
def javadocsfinder(jar):
    for i in jar:
        if i[0] == jar[0][0] or i[0] in ["", "../"]:
            continue
        if i[0].partition("javadoc")[1]:
            javadocs = i[0]
            return javadocs
            break


#print(url)
def downloadandextract(url, path):
    if readall(f"javadocs/{path}/version.txt")[0].rstrip() == url:
        print(path, "versions match ")
        return "verions match"
    urllib.request.urlretrieve(url, "temp.zip")
    with zipfile.ZipFile("temp.zip", 'r') as zip_ref:
        zip_ref.extractall(f'javadocs/{path}')
    os.remove("temp.zip")
    writelist([url], f"javadocs/{path}/version.txt")

def main(url, path, version="unused"):
    masterlist = dateandtitlegetter(url)
    url = sortdir(masterlist, url, version)
    jar = dateandtitlegetter(url)
    url = url + javadocsfinder(jar)
    downloadandextract(url, path)