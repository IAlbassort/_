from init import writelist
from init import readall
import sys
sys.path.insert(1, '../')
import os
from pathlib import Path
#wanna put this in /python but thats kinda annoying to death with 
interfaces = []
header = ["package net.albassort.station", "import jnr.ffi.LibraryLoader", "import net.albassort.station.gettempdir",
          "import net.albassort.station.libmodify", "class libclass{"]
cbt = []
endlist = []
formalnamelist = []
for file in os.listdir("src/main/nim"):
    if not file.partition(".nim")[1]:
        continue
    formalname = file.partition(".nim")[0]
    formalnamelist.append("#"+formalname)
    cbt.append(" "*7+"fun lib"+formalname+"() : "+formalname+"{")
    cbt.append(" "*10+f"return LibraryLoader.create({formalname}::class.java).load" +
               '("${gettempdir()}/${libmodify('+f'"{formalname}"'+')}")'+"}")
    interfaces.append(f"   public interface "+formalname+"{")
    x = readall(f"src/main/nim/{file}")


    headerlist = [] 
    for i in x:
        # for some reason find is inverted, it should not need the "not" bultin, smh.
        if not i.find("proc"):
            if i.partition("proc priv_")[1]:
                continue
            name = i.partition("proc ")[2].partition("(")[0]
            param = i.partition(name)[2].partition(
                "=")[0].partition("{.")[0].rstrip()
            if not param.partition(") : ")[1]:
                type = "void"
            else:
                type = param.partition(") :")[2][1:]
            if param[1] == ")":
                param = "()"
            elif type != "void":
                param = param.rpartition(" :")[0]
            headerlist.append((name, type, param))
    # this searches the file for functions and gets their name, type, and paramaters

    typedict = {"cstring": "String", "int": "Int",
                "float": "Float", "string": "String"}
    for i in headerlist:

        temp = []
        type = i[1]
        name = i[0]
        param = i[2]
        # this formats them to kotlin standards)
        if param != "()":
            vars = param.split(",")
            counter = 1
            if len(vars) == 1:
                singlevar = True
            else:
                singlevar = False
            for i in vars:
                if i in [",", ""]:
                    continue
                i = i.lstrip().partition(")")[0].partition(":")
                temp.append(f'{i[0]} : {typedict[i[2].lstrip().rstrip()]}')
                if singlevar:
                    temp.append(")")
                elif counter != len(vars)-vars.count(","):
                    temp.append(", ")
                else:
                    temp.append(")")
                counter = counter+1
            param = "".join(temp)
        if param == "()":
            interfaces.append((f"       fun {name}()"))
        elif type != "void":
            interfaces.append(f"       fun {name}{param} : {typedict[type]}")
        else:
            interfaces.append((f"       fun {name}{param}"))
    interfaces.append("   }")
for i in endlist:
    for i in i:
        print(i)
cbt.insert(0, " "*4+"companion object {")
interfaces.append("}")
cbt.append(" "*4+ "}")

for i in cbt:
    header.append(i)
for i in interfaces:
    header.append(i) 
for i in header:
    print(i)
os.remove("src/main/kotlin/net/albassort/station/libinterface.kt")
writelist(header, "src/main/kotlin/net/albassort/station/libinterface.kt")
nimmeta = readall(f"bin/main/nimmeta.base")

print(nimmeta)
for i in formalnamelist:
    nimmeta.append(i)
if os.path.exists("resources/nimmeta.txt"):
    os.remove("resources/nimmeta.txt")
writelist(nimmeta, 'resources/nimmeta.txt')
