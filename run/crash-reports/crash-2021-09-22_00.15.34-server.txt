---- Minecraft Crash Report ----
// Hey, that tickles! Hehehe!

Time: 9/22/21, 12:15 AM
Description: Exception in server tick loop

org.spongepowered.asm.mixin.transformer.throwables.MixinTransformerError: An unexpected critical error was encountered
	at org.spongepowered.asm.mixin.transformer.MixinProcessor.applyMixins(MixinProcessor.java:363)
	at org.spongepowered.asm.mixin.transformer.MixinTransformer.transformClass(MixinTransformer.java:208)
	at org.spongepowered.asm.mixin.transformer.MixinTransformer.transformClassBytes(MixinTransformer.java:178)
	at org.spongepowered.asm.mixin.transformer.FabricMixinTransformerProxy.transformClassBytes(FabricMixinTransformerProxy.java:23)
	at net.fabricmc.loader.launch.knot.KnotClassDelegate.getPostMixinClassByteArray(KnotClassDelegate.java:157)
	at net.fabricmc.loader.launch.knot.KnotClassLoader.loadClass(KnotClassLoader.java:150)
	at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:519)
	at net.minecraft.entity.player.PlayerEntity.<init>(PlayerEntity.java:168)
	at net.minecraft.server.network.ServerPlayerEntity.<init>(ServerPlayerEntity.java:194)
	at net.minecraft.server.PlayerManager.createPlayer(PlayerManager.java:426)
	at net.minecraft.server.network.ServerLoginNetworkHandler.acceptPlayer(ServerLoginNetworkHandler.java:153)
	at net.minecraft.server.network.ServerLoginNetworkHandler.redirect$zlh000$handlePlayerJoin(ServerLoginNetworkHandler.java:563)
	at net.minecraft.server.network.ServerLoginNetworkHandler.tick(ServerLoginNetworkHandler.java:94)
	at net.minecraft.network.ClientConnection.tick(ClientConnection.java:250)
	at net.minecraft.server.ServerNetworkIo.tick(ServerNetworkIo.java:153)
	at net.minecraft.server.MinecraftServer.tickWorlds(MinecraftServer.java:961)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:887)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:88)
	at net.minecraft.server.MinecraftServer.runServer(MinecraftServer.java:744)
	at net.minecraft.server.MinecraftServer.method_29739(MinecraftServer.java:276)
	at java.base/java.lang.Thread.run(Thread.java:831)
Caused by: org.spongepowered.asm.mixin.throwables.MixinApplyError: Mixin [retstation.mixins.json:EatMixin] from phase [DEFAULT] in config [retstation.mixins.json] FAILED during APPLY
	at org.spongepowered.asm.mixin.transformer.MixinProcessor.handleMixinError(MixinProcessor.java:642)
	at org.spongepowered.asm.mixin.transformer.MixinProcessor.handleMixinApplyError(MixinProcessor.java:594)
	at org.spongepowered.asm.mixin.transformer.MixinProcessor.applyMixins(MixinProcessor.java:356)
	... 20 more
Caused by: org.spongepowered.asm.mixin.transformer.throwables.InvalidMixinException: Mixin retstation.mixins.json:EatMixin contains non-private static method test(Lnet/minecraft/item/Item;Lnet/minecraft/item/ItemStack;Lorg/spongepowered/asm/mixin/injection/callback/CallbackInfo;)V [ -> MAIN Applicator Phase -> retstation.mixins.json:EatMixin -> Apply Methods -> (Lnet/minecraft/item/Item;Lnet/minecraft/item/ItemStack;Lorg/spongepowered/asm/mixin/injection/callback/CallbackInfo;)V:handler$zzc000$test]
	at org.spongepowered.asm.mixin.transformer.MixinApplicatorStandard.checkMethodVisibility(MixinApplicatorStandard.java:1071)
	at org.spongepowered.asm.mixin.transformer.MixinApplicatorStandard.applyNormalMethod(MixinApplicatorStandard.java:526)
	at org.spongepowered.asm.mixin.transformer.MixinApplicatorStandard.applyMethods(MixinApplicatorStandard.java:509)
	at org.spongepowered.asm.mixin.transformer.MixinApplicatorStandard.applyMixin(MixinApplicatorStandard.java:381)
	at org.spongepowered.asm.mixin.transformer.MixinApplicatorStandard.apply(MixinApplicatorStandard.java:320)
	at org.spongepowered.asm.mixin.transformer.TargetClassContext.applyMixins(TargetClassContext.java:345)
	at org.spongepowered.asm.mixin.transformer.MixinProcessor.applyMixins(MixinProcessor.java:569)
	at org.spongepowered.asm.mixin.transformer.MixinProcessor.applyMixins(MixinProcessor.java:351)
	... 20 more


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.17
	Minecraft Version ID: 1.17
	Operating System: Linux (amd64) version 5.13.12-arch1-1
	Java Version: 16.0.2, N/A
	Java VM Version: OpenJDK 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 606171328 bytes (578 MiB) / 2065694720 bytes (1970 MiB) up to 2065694720 bytes (1970 MiB)
	CPUs: 4
	Processor Vendor: GenuineIntel
	Processor Name: Intel(R) Core(TM) i5-4570S CPU @ 2.90GHz
	Identifier: Intel64 Family 6 Model 60 Stepping 3
	Microarchitecture: unknown
	Frequency (GHz): 2.90
	Number of physical packages: 1
	Number of physical CPUs: 4
	Number of logical CPUs: 4
	Graphics card #0 name: Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller
	Graphics card #0 vendor: Intel Corporation (0x8086)
	Graphics card #0 VRAM (MB): 256.00
	Graphics card #0 deviceId: 0x0412
	Graphics card #0 versionInfo: unknown
	Virtual memory max (MB): 14022.11
	Virtual memory used (MB): 12207.95
	Swap memory total (MB): 10083.00
	Swap memory used (MB): 4525.54
	JVM Flags: 1 total; -XX:+ShowCodeDetailsInExceptionMessages
	Fabric Mods: 
		fabric: Fabric API 0.34.10+1.17
		fabric-api-base: Fabric API Base 0.3.0+a02b44633d
		fabric-api-lookup-api-v1: Fabric API Lookup API (v1) 1.0.2+a02b44633d
		fabric-biome-api-v1: Fabric Biome API (v1) 3.1.11+9e521e133d
		fabric-blockrenderlayer-v1: Fabric BlockRenderLayer Registration (v1) 1.1.5+a02b44633d
		fabric-command-api-v1: Fabric Command API (v1) 1.1.1+a02b44633d
		fabric-commands-v0: Fabric Commands (v0) 0.2.2+92519afa3d
		fabric-containers-v0: Fabric Containers (v0) 0.1.12+a02b44633d
		fabric-content-registries-v0: Fabric Content Registries (v0) 0.2.2+a02b44633d
		fabric-crash-report-info-v1: Fabric Crash Report Info (v1) 0.1.5+be9da3103d
		fabric-dimensions-v1: Fabric Dimensions API (v1) 2.0.10+a02b44633d
		fabric-entity-events-v1: Fabric Entity Events (v1) 1.1.0+a02b44633d
		fabric-events-interaction-v0: Fabric Events Interaction (v0) 0.4.8+a02b44633d
		fabric-events-lifecycle-v0: Fabric Events Lifecycle (v0) 0.2.1+92519afa3d
		fabric-game-rule-api-v1: Fabric Game Rule API (v1) 1.0.6+a02b44633d
		fabric-item-api-v1: Fabric Item API (v1) 1.2.4+a02b44633d
		fabric-item-groups-v0: Fabric Item Groups (v0) 0.2.10+b7ab61213d
		fabric-key-binding-api-v1: Fabric Key Binding API (v1) 1.0.4+a02b44633d
		fabric-keybindings-v0: Fabric Key Bindings (v0) 0.2.2+36b77c3e3d
		fabric-language-kotlin: Fabric Language Kotlin 1.6.0+kotlin.1.5.0
		fabric-lifecycle-events-v1: Fabric Lifecycle Events (v1) 1.4.4+a02b44633d
		fabric-loot-tables-v1: Fabric Loot Tables (v1) 1.0.4+a02b44633d
		fabric-mining-levels-v0: Fabric Mining Levels (v0) 0.1.3+92519afa3d
		fabric-models-v0: Fabric Models (v0) 0.3.0+a02b44633d
		fabric-networking-api-v1: Fabric Networking API (v1) 1.0.11+b7ab61213d
		fabric-networking-blockentity-v0: Fabric Networking Block Entity (v0) 0.2.11+a02b44633d
		fabric-networking-v0: Fabric Networking (v0) 0.3.2+92519afa3d
		fabric-object-builder-api-v1: Fabric Object Builder API (v1) 1.10.9+b7ab61213d
		fabric-object-builders-v0: Fabric Object Builders (v0) 0.7.3+a02b44633d
		fabric-particles-v1: Fabric Particles (v1) 0.2.4+a02b44633d
		fabric-registry-sync-v0: Fabric Registry Sync (v0) 0.7.9+a02b44633d
		fabric-renderer-api-v1: Fabric Renderer API (v1) 0.4.3+676f40fa3d
		fabric-renderer-indigo: Fabric Renderer - Indigo 0.4.8+a02b44633d
		fabric-renderer-registries-v1: Fabric Renderer Registries (v1) 3.2.0+a02b44633d
		fabric-rendering-data-attachment-v1: Fabric Rendering Data Attachment (v1) 0.1.5+a02b44633d
		fabric-rendering-fluids-v1: Fabric Rendering Fluids (v1) 0.1.13+a02b44633d
		fabric-rendering-v0: Fabric Rendering (v0) 1.1.2+92519afa3d
		fabric-rendering-v1: Fabric Rendering (v1) 1.6.0+a02b44633d
		fabric-resource-loader-v0: Fabric Resource Loader (v0) 0.4.7+b7ab61213d
		fabric-screen-api-v1: Fabric Screen API (v1) 1.0.3+b7ab61213d
		fabric-screen-handler-api-v1: Fabric Screen Handler API (v1) 1.1.8+a02b44633d
		fabric-structure-api-v1: Fabric Structure API (v1) 1.1.10+be9da3103d
		fabric-tag-extensions-v0: Fabric Tag Extensions (v0) 1.1.4+a02b44633d
		fabric-textures-v0: Fabric Textures (v0) 1.0.6+a02b44633d
		fabric-tool-attribute-api-v1: Fabric Tool Attribute API (v1) 1.2.12+b7ab61213d
		fabricloader: Fabric Loader 0.11.3
		hydrogen: Hydrogen 0.3
		java: OpenJDK 64-Bit Server VM 16
		lithium: Lithium 0.7.4
		minecraft: Minecraft 1.17
		retstation: RetStation Mod ALPHA
		smoothboot: Smooth Boot 1.16.5-1.6.0
	Player Count: 0 / 8; []
	Data Packs: vanilla, Fabric Mods
	Type: Integrated Server (map_client.txt)
	Is Modded: Definitely; Client brand changed to 'fabric'