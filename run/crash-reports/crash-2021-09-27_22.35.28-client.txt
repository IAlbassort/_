---- Minecraft Crash Report ----
// This doesn't make any sense!

Time: 9/27/21, 10:35 PM
Description: Unexpected error

java.lang.NullPointerException: Cannot invoke "net.albassort.station.interfaces.Interfaces$Drugs$DrugNames.getHalfLife()" because the return value of "net.albassort.station.interfaces.Interfaces$Drugs$DrugEdible.getDrugName()" is null
	at net.albassort.station.interfaces.Interfaces$Drugs$DrugEdible.<init>(Interfaces.java:134)
	at net.albassort.station.interfaces.Interfaces$Drugs$DrugEdible.duplicateDrug(Interfaces.java:151)
	at net.albassort.station.register.ItemRegister$Pill.use(ItemRegister.java:28)
	at net.minecraft.item.ItemStack.use(ItemStack.java:227)
	at net.minecraft.client.network.ClientPlayerInteractionManager.interactItem(ClientPlayerInteractionManager.java:321)
	at net.minecraft.client.MinecraftClient.doItemUse(MinecraftClient.java:1693)
	at net.minecraft.client.MinecraftClient.handleInputEvents(MinecraftClient.java:1948)
	at net.minecraft.client.MinecraftClient.tick(MinecraftClient.java:1762)
	at net.minecraft.client.MinecraftClient.render(MinecraftClient.java:1125)
	at net.minecraft.client.MinecraftClient.run(MinecraftClient.java:746)
	at net.minecraft.client.main.Main.main(Main.java:191)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:78)
	at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.base/java.lang.reflect.Method.invoke(Method.java:567)
	at net.fabricmc.loader.game.MinecraftGameProvider.launch(MinecraftGameProvider.java:226)
	at net.fabricmc.loader.launch.knot.Knot.launch(Knot.java:146)
	at net.fabricmc.loader.launch.knot.KnotClient.main(KnotClient.java:28)
	at net.fabricmc.devlaunchinjector.Main.main(Main.java:86)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Render thread
Stacktrace:
	at net.albassort.station.interfaces.Interfaces$Drugs$DrugEdible.<init>(Interfaces.java:134)
	at net.albassort.station.interfaces.Interfaces$Drugs$DrugEdible.duplicateDrug(Interfaces.java:151)
	at net.albassort.station.register.ItemRegister$Pill.use(ItemRegister.java:28)
	at net.minecraft.item.ItemStack.use(ItemStack.java:227)
	at net.minecraft.client.network.ClientPlayerInteractionManager.interactItem(ClientPlayerInteractionManager.java:321)
	at net.minecraft.client.MinecraftClient.doItemUse(MinecraftClient.java:1693)
	at net.minecraft.client.MinecraftClient.handleInputEvents(MinecraftClient.java:1948)

-- Affected level --
Details:
	All players: 1 total; [ClientPlayerEntity['Player543'/1, l='ClientLevel', x=9.43, y=4.00, z=8.13]]
	Chunk stats: 841, 599
	Level dimension: minecraft:overworld
	Level spawn location: World: (0,4,0), Section: (at 0,4,0 in 0,0,0; chunk contains blocks 0,0,0 to 15,255,15), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 93371 game time, 93371 day time
	Server brand: fabric
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.world.ClientWorld.addDetailsToCrashReport(ClientWorld.java:365)
	at net.minecraft.client.MinecraftClient.addDetailsToCrashReport(MinecraftClient.java:2416)
	at net.minecraft.client.MinecraftClient.run(MinecraftClient.java:768)
	at net.minecraft.client.main.Main.main(Main.java:191)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:78)
	at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.base/java.lang.reflect.Method.invoke(Method.java:567)
	at net.fabricmc.loader.game.MinecraftGameProvider.launch(MinecraftGameProvider.java:226)
	at net.fabricmc.loader.launch.knot.Knot.launch(Knot.java:146)
	at net.fabricmc.loader.launch.knot.KnotClient.main(KnotClient.java:28)
	at net.fabricmc.devlaunchinjector.Main.main(Main.java:86)

-- Last reload --
Details:
	Reload number: 1
	Reload reason: initial
	Finished: Yes
	Packs: Default, Fabric Mods

-- System Details --
Details:
	Minecraft Version: 1.17
	Minecraft Version ID: 1.17
	Operating System: Linux (amd64) version 5.13.12-arch1-1
	Java Version: 16.0.2, N/A
	Java VM Version: OpenJDK 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 305121384 bytes (290 MiB) / 1698693120 bytes (1620 MiB) up to 2065694720 bytes (1970 MiB)
	CPUs: 4
	Processor Vendor: GenuineIntel
	Processor Name: Intel(R) Core(TM) i5-4570S CPU @ 2.90GHz
	Identifier: Intel64 Family 6 Model 60 Stepping 3
	Microarchitecture: unknown
	Frequency (GHz): 2.90
	Number of physical packages: 1
	Number of physical CPUs: 4
	Number of logical CPUs: 4
	Graphics card #0 name: Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller
	Graphics card #0 vendor: Intel Corporation (0x8086)
	Graphics card #0 VRAM (MB): 256.00
	Graphics card #0 deviceId: 0x0412
	Graphics card #0 versionInfo: unknown
	Virtual memory max (MB): 14022.11
	Virtual memory used (MB): 12706.08
	Swap memory total (MB): 10083.00
	Swap memory used (MB): 5027.11
	JVM Flags: 1 total; -XX:+ShowCodeDetailsInExceptionMessages
	Fabric Mods: 
		fabric: Fabric API 0.34.10+1.17
		fabric-api-base: Fabric API Base 0.3.0+a02b44633d
		fabric-api-lookup-api-v1: Fabric API Lookup API (v1) 1.0.2+a02b44633d
		fabric-biome-api-v1: Fabric Biome API (v1) 3.1.11+9e521e133d
		fabric-blockrenderlayer-v1: Fabric BlockRenderLayer Registration (v1) 1.1.5+a02b44633d
		fabric-command-api-v1: Fabric Command API (v1) 1.1.1+a02b44633d
		fabric-commands-v0: Fabric Commands (v0) 0.2.2+92519afa3d
		fabric-containers-v0: Fabric Containers (v0) 0.1.12+a02b44633d
		fabric-content-registries-v0: Fabric Content Registries (v0) 0.2.2+a02b44633d
		fabric-crash-report-info-v1: Fabric Crash Report Info (v1) 0.1.5+be9da3103d
		fabric-dimensions-v1: Fabric Dimensions API (v1) 2.0.10+a02b44633d
		fabric-entity-events-v1: Fabric Entity Events (v1) 1.1.0+a02b44633d
		fabric-events-interaction-v0: Fabric Events Interaction (v0) 0.4.8+a02b44633d
		fabric-events-lifecycle-v0: Fabric Events Lifecycle (v0) 0.2.1+92519afa3d
		fabric-game-rule-api-v1: Fabric Game Rule API (v1) 1.0.6+a02b44633d
		fabric-item-api-v1: Fabric Item API (v1) 1.2.4+a02b44633d
		fabric-item-groups-v0: Fabric Item Groups (v0) 0.2.10+b7ab61213d
		fabric-key-binding-api-v1: Fabric Key Binding API (v1) 1.0.4+a02b44633d
		fabric-keybindings-v0: Fabric Key Bindings (v0) 0.2.2+36b77c3e3d
		fabric-language-kotlin: Fabric Language Kotlin 1.6.0+kotlin.1.5.0
		fabric-lifecycle-events-v1: Fabric Lifecycle Events (v1) 1.4.4+a02b44633d
		fabric-loot-tables-v1: Fabric Loot Tables (v1) 1.0.4+a02b44633d
		fabric-mining-levels-v0: Fabric Mining Levels (v0) 0.1.3+92519afa3d
		fabric-models-v0: Fabric Models (v0) 0.3.0+a02b44633d
		fabric-networking-api-v1: Fabric Networking API (v1) 1.0.11+b7ab61213d
		fabric-networking-blockentity-v0: Fabric Networking Block Entity (v0) 0.2.11+a02b44633d
		fabric-networking-v0: Fabric Networking (v0) 0.3.2+92519afa3d
		fabric-object-builder-api-v1: Fabric Object Builder API (v1) 1.10.9+b7ab61213d
		fabric-object-builders-v0: Fabric Object Builders (v0) 0.7.3+a02b44633d
		fabric-particles-v1: Fabric Particles (v1) 0.2.4+a02b44633d
		fabric-registry-sync-v0: Fabric Registry Sync (v0) 0.7.9+a02b44633d
		fabric-renderer-api-v1: Fabric Renderer API (v1) 0.4.3+676f40fa3d
		fabric-renderer-indigo: Fabric Renderer - Indigo 0.4.8+a02b44633d
		fabric-renderer-registries-v1: Fabric Renderer Registries (v1) 3.2.0+a02b44633d
		fabric-rendering-data-attachment-v1: Fabric Rendering Data Attachment (v1) 0.1.5+a02b44633d
		fabric-rendering-fluids-v1: Fabric Rendering Fluids (v1) 0.1.13+a02b44633d
		fabric-rendering-v0: Fabric Rendering (v0) 1.1.2+92519afa3d
		fabric-rendering-v1: Fabric Rendering (v1) 1.6.0+a02b44633d
		fabric-resource-loader-v0: Fabric Resource Loader (v0) 0.4.7+b7ab61213d
		fabric-screen-api-v1: Fabric Screen API (v1) 1.0.3+b7ab61213d
		fabric-screen-handler-api-v1: Fabric Screen Handler API (v1) 1.1.8+a02b44633d
		fabric-structure-api-v1: Fabric Structure API (v1) 1.1.10+be9da3103d
		fabric-tag-extensions-v0: Fabric Tag Extensions (v0) 1.1.4+a02b44633d
		fabric-textures-v0: Fabric Textures (v0) 1.0.6+a02b44633d
		fabric-tool-attribute-api-v1: Fabric Tool Attribute API (v1) 1.2.12+b7ab61213d
		fabricloader: Fabric Loader 0.11.3
		hydrogen: Hydrogen 0.3
		java: OpenJDK 64-Bit Server VM 16
		lithium: Lithium 0.7.4
		minecraft: Minecraft 1.17
		retstation: RetStation Mod ALPHA
		smoothboot: Smooth Boot 1.16.5-1.6.0
	Launched Version: Fabric
	Backend library: LWJGL version 3.2.2 build 10
	Backend API: Mesa DRI Intel(R) HD Graphics 4600 (HSW GT2) GL version 4.5 (Core Profile) Mesa 21.2.1, Intel Open Source Technology Center
	Window size: 854x480
	GL Caps: Using framebuffer using OpenGL 3.2
	GL debug messages: 
	Using VBOs: Yes
	Is Modded: Definitely; Client brand changed to 'fabric'
	Type: Integrated Server (map_client.txt)
	Graphics mode: fancy
	Resource Packs: Fabric Mods
	Current Language: English (US)
	CPU: 4x Intel(R) Core(TM) i5-4570S CPU @ 2.90GHz
	Player Count: 1 / 8; [ServerPlayerEntity['Player543'/1, l='ServerLevel[podrfguiosdgjodfijg]', x=9.43, y=4.00, z=8.13]]
	Data Packs: vanilla, Fabric Mods