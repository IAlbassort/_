package net.albassort.station.interfaces;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Util;
import net.minecraft.text.LiteralText;

import java.util.List;

public class Interfaces{
    public interface DamageInterface{
    //Sets up the mutability of the various damage types;P
    //takes input from NewDamage
    float getdamage(NewDamage input) throws Exception;
    boolean changedamage(NewDamage input, float inputfloat) throws Exception;
    float setdamage(NewDamage input, float inputfloat) throws Exception;
    public void addReagent(Drugs.DrugEdible drug);
    public void removeReagent(Drugs.DrugEdible drug);
    public List<Drugs.DrugEdible> listReagents();
    public void overRideReagents(List<Interfaces.Drugs.DrugEdible> override);


        public class Containers{
//contains for kotlin to intercace with
        public static float getdamage(LivingEntity instance, NewDamage Type) throws Exception {
            return ((DamageInterface)instance).getdamage(Type);}

        public static boolean changedamage(LivingEntity instance, NewDamage Type, float inputfloat) throws Exception {
            return ((DamageInterface)instance).changedamage(Type, inputfloat);}

        public static float setdamage(LivingEntity instance, NewDamage Type, float inputfloat) throws Exception{
            return ((DamageInterface)instance).setdamage(Type, inputfloat);}

        public static void addReagent(PlayerEntity instance, Drugs.DrugEdible drug){
            ((DamageInterface)instance).addReagent(drug);
        }
        public static void removeReagent(PlayerEntity instance, Drugs.DrugEdible drug){
            ((DamageInterface)instance).removeReagent(drug);
        }
        public static void overRideReagents(PlayerEntity instance, List<Interfaces.Drugs.DrugEdible> override){
            ((DamageInterface)instance).overRideReagents(override);
        }
        public static List<Drugs.DrugEdible> listReagents(PlayerEntity instance){
            return ((DamageInterface)instance).listReagents();}
            }

        }
    public interface DrugInterface{
        public boolean isDrug(Item item);
        public class Containers{
            public static boolean isDrug(Item item){
                return ((DrugInterface)item).isDrug(item);
            }
        }
    }

    public enum NewDamage {
    burn("burn"),
    suff("suff"),
    toxin("toxin"),
    brute("brute"),
    total("total");

    public final String name;
    private NewDamage(String name) {
      this.name = name;}
    }

    //insert new drugs types here.
    public static class Drugs{
      public enum DrugNames {

      deathpill("deathpill", 1000);

      public float getHalfLife(){ return this.halflife; }

      public String name;
      public float halflife;
      private DrugNames(String name, float halflife) {
        this.name = name;
        this.halflife =halflife;
        }
      }
        //creates an edible drug, from this dosage and name.
        public static class DrugEdible {
            public DrugEdible(DrugNames name, float dosage){
                this.drug = name;
                this.halflifecounter = drug.halflife;
                this.dosage = dosage;}

            final DrugNames drug;
            float dosage;
            float halflifecounter;

            public float getDosage() { return this.dosage; }

            public DrugNames getDrugName() { return this.drug; }

            public void halfLife() {
                this.dosage = (this.dosage/2);
                this.halflifecounter = this.drug.getHalfLife();
            }

            public float getHalfLife(){ return this.halflifecounter; }

            public void halfLifeTick(int input){
                halflifecounter=-input;
                if (halflifecounter <= 0){ halfLife(); }
            }
            public DrugEdible duplicateDrug(){ return new DrugEdible(this.drug, this.dosage); }
        }

      public static class DrugFunc{
          //functions associated with each drug.
        public static void deathpill(float dosage, Entity entity){
          if (dosage >= 50){ entity.kill(); }
        if (dosage >= 50){ entity.damage(DamageSource.GENERIC, 5); }
        }
        public static void sugarpill(Entity entity){
          if (entity.isPlayer()){
              entity.sendSystemMessage(new LiteralText("You feel your blood sugar rise..."), Util.NIL_UUID);
          }
        }
        //temporary thing, looking for a better situation...
      public static boolean drugswitch(DrugEdible function, Entity entity){
        switch (function.getDrugName()){
          case deathpill: deathpill(function.getDosage(), entity); return true;
          default: sugarpill(entity); return false;}
        }
      }
    
  }
}