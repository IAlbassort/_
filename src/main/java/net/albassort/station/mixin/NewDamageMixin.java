package net.albassort.station.mixin;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import net.albassort.station.interfaces.Interfaces.DamageInterface;
import net.albassort.station.interfaces.Interfaces.NewDamage;
import net.albassort.station.interfaces.Interfaces.Drugs.DrugFunc;
import net.albassort.station.interfaces.Interfaces.Drugs;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.world.World;
import java.util.ArrayList;
import java.util.List;
// Mixins HAVE to be written in java due to constraints in the mixin system.

//First attempt for a mixin, it simply adds new counters to all living entities.
@Mixin(LivingEntity.class)
public abstract class NewDamageMixin extends Entity implements DamageInterface {
    public NewDamageMixin(EntityType<?> type, World world) {
        super(type, world);
    }

    float burn_float = 0;
    float suff_float = 0;
    float toxic_float = 0;
    float brute_float = 0;

    private float sumdam() {
        return burn_float + suff_float + toxic_float + brute_float;
    }

    // these are injected at the start of livijg entities
    float total = (sumdam());

    // we use the duck interface method so it can be used outside in other
    // functions, this is mere the injection spot.
    @Override
    // returns the damage per each types!
    public float getdamage(NewDamage input) throws Exception {
        return switch (input) {
            // needs to use -> in return switch
            case brute -> brute_float;
            case burn -> burn_float;
            case toxin -> toxic_float;
            case suff -> suff_float;
            case total -> sumdam();
        };
    }

    @Override
    // enums allow me to work depending on 1 of the 5 inputs
    public boolean changedamage(NewDamage input, float inputfloat) throws Exception {

        switch (input) {
            case brute -> brute_float = brute_float + inputfloat;
            case burn -> burn_float = burn_float + inputfloat;
            case toxin -> toxic_float = toxic_float + inputfloat;
            case suff -> suff_float = suff_float + inputfloat;
            case total -> throw new Exception("Please do not pass total into here.");
        }
        if (sumdam() <= -100) {
            // if your damage gets to -100, you literally just die
            return this.damage(DamageSource.GENERIC, Float.MAX_VALUE);
        }
        return false;
    }

    @Override
    public float setdamage(NewDamage input, float inputfloat) throws Exception {
        // sets the damage to a fixed amount
        switch (input) {
            case brute:
                brute_float = inputfloat;
                return brute_float;
            case burn:
                burn_float = inputfloat;
                return burn_float;
            case toxin:
                toxic_float = inputfloat;
                return toxic_float;
            case suff:
                suff_float = inputfloat;
                return suff_float;
            case total:
                throw new Exception("Please donot pass total into here.");
            default:
                throw new Exception("ERROR SOMEHOW");
        }
    }
    // @Override
    // public void swallowdrug(Drugs.DrugNames drug, float dosage){
    // DrugFunc.drugswitch(drug, this, dosage);
    // }
    
    //this was for debugging, and is no longer needed.
    
    /*
    @Inject(method = "damage", at = @At(ordinal = 0, value = "RETURN"), cancellable = true)
    private void test1(CallbackInfoReturnable ci) {
        System.out.println("1");
    }

    @Inject(method = "damage", at = @At(ordinal = 1, value = "RETURN"), cancellable = true)
    private void test2(CallbackInfoReturnable ci) {
        System.out.println("2");
    }

    @Inject(method = "damage", at = @At(ordinal = 2, value = "RETURN"), cancellable = true)
    private void test3(CallbackInfoReturnable ci) {
        System.out.println("3");
    }

    @Inject(method = "damage", at = @At(ordinal = 3, value = "RETURN"), cancellable = true)
    private void test4(CallbackInfoReturnable ci) {
        System.out.println("4");
    }

    @Inject(method = "damage", at = @At(ordinal = 4, value = "RETURN"), cancellable = true)
    private void test5(CallbackInfoReturnable ci) {
        System.out.println("5");
    }

    @Inject(method = "damage", at = @At("RETURN"), cancellable = true)
    private void test6(CallbackInfoReturnable ci) {
        System.out.println("all");
    }
    */
}
