package net.albassort.station.mixin;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.albassort.station.interfaces.Interfaces;
import org.spongepowered.asm.mixin.Mixin;

import java.util.ArrayList;
import java.util.List;

@Mixin(PlayerEntity.class)
//this adds the functions to manipulate the counters as established in newdamage mixin :)
public abstract class ServerPlayerEntityMixin implements Interfaces.DamageInterface {
    List<Interfaces.Drugs.DrugEdible> activereagents = new ArrayList<>();
    @Override
    public void addReagent(Interfaces.Drugs.DrugEdible drug){ activereagents.add(drug);}
    @Override
    public void removeReagent(Interfaces.Drugs.DrugEdible drug)
    { activereagents.remove(drug); }

    @Override
    public List<Interfaces.Drugs.DrugEdible> listReagents(){ return activereagents; }
    @Override
    public void overRideReagents(List<Interfaces.Drugs.DrugEdible> override){ activereagents = override; }
}

