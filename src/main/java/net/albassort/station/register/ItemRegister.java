package net.albassort.station.register;
import com.mojang.datafixers.util.Pair;
import jnr.ffi.StructLayout;
import net.albassort.station.interfaces.Interfaces;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.albassort.station.interfaces.Interfaces.Drugs.DrugNames;
import net.albassort.station.interfaces.Interfaces.Drugs.DrugEdible;
import net.albassort.station.interfaces.Interfaces.Drugs.DrugFunc;
import net.albassort.station.interfaces.Interfaces.DamageInterface.Containers;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.spongepowered.asm.mixin.Unique;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class ItemRegister {


    static class foodtest extends FoodComponents{
        public static FoodComponent GENERICDRUG;
        static {GENERICDRUG = (new FoodComponent.Builder()
                .snack().alwaysEdible().build());}
    }
    //registers a pill item
    public static class Pill extends Item{

        public ActionResult useOnBlock(ItemUsageContext context){
            //upon usage it adds the Reagent to the player. duplicating it because otherwise
            //it will modify the drugs actions
            Containers.addReagent(context.getPlayer(), this.drug.duplicateDrug());
            System.out.println("this is a test, only a test");
            //removes the item from your hand
            context.getStack().decrement(1);
            //tells the callback that the action resulted correctly.
            return ActionResult.SUCCESS;
        }

        public final DrugEdible drug;
        public Pill(DrugEdible drug, Settings settings){
            super(settings);
            this.drug = drug;
        }
        public DrugNames getDrugName() { return this.drug.getDrugName(); }
        public float getDosage() { return this.drug.getDosage(); }
        public DrugEdible getDrug() { return this.drug; }
        //stolen directly from HoeItem.java
        }  
        //interfaces withfabrics systems to actually insert the items into the game
    public static Pill FABRIC_ITEM = new Pill(new DrugEdible(DrugNames.deathpill, 49),
            new Item.Settings().group(ItemGroup.MISC)
                    .maxCount(1));
    public static Pill FABRIC_ITEM2 = new Pill(new DrugEdible(DrugNames.deathpill, 51),
            new Item.Settings().group(ItemGroup.MISC)
                    .maxCount(1));
    //registerItem(Constant.Item.LARGE_OXYGEN_TANK, new OxygenTankItem(new Item.Settings().group(ITEMS_GROUP), 1620 * 30)); //48600 ticks
}





