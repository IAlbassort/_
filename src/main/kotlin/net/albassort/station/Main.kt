package net.albassort.station
import net.albassort.station.interfaces.Interfaces.*
import net.albassort.station.interfaces.Interfaces.Drugs.DrugFunc.drugswitch
import net.albassort.station.interfaces.Interfaces.DamageInterface.Containers
import net.albassort.station.register.ItemRegister
import net.albassort.station.register.ItemRegister.*
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents
import net.fabricmc.fabric.api.event.player.AttackBlockCallback
import net.fabricmc.fabric.api.event.registry.*
import net.fabricmc.fabric.api.networking.v1.PlayerLookup
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.damage.*
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.util.registry.Registry
import net.minecraft.world.World
import org.spongepowered.asm.mixin.Mutable

// For support join https://discord.gg/v6v4pMv
//registering here because why not
@Suppress("unused")
fun init(){
    var counter : Int = 0
    val everytick : Int = 40
    println("hello uwu")
    Registry.register(Registry.ITEM, Identifier("tutorial", "fabric_item"), ItemRegister.FABRIC_ITEM)
    Registry.register(Registry.ITEM, Identifier("tutorial2", "fabric_item"), ItemRegister.FABRIC_ITEM2)
    //holy shit...
    //val playerReagents : MutableList<Pair<ServerPlayerEntity, MutableList<Drugs.DrugEdiblez`>>> =
      //  arrayOf<Pair<ServerPlayerEntity, MutableList<Drugs.DrugEdible>>>().toMutableList()
    //MUDA MUDA MUDA
    ServerTickEvents.START_SERVER_TICK.register(ServerTickEvents.StartTick {
        if (counter == 0){
            //writing slow code to touchup later!
            for (player in PlayerLookup.all(it)){
//                val reagents : MutableList<Drugs.DrugEdible> = player.listReagents()
                val reagents = player.listReagents()
                val totalreagents : MutableList<Pair<Drugs.DrugNames, MutableList<Float>>>
                = arrayOf<Pair<Drugs.DrugNames, MutableList<Float>>>().toMutableList()
                if (reagents.isEmpty()){ continue }
                try{
                    //this compiles a list of drugs and their total dosages
                    for (drugit in 0 until reagents.size) {
                        val drug = reagents[drugit]
                        for (drugs in reagents){
                            if (totalreagents.isEmpty()) {
                                totalreagents.add(Pair(drug.drugName, arrayOf<Float>().toMutableList()))
                                continue
                            }
                            for (druglist in 0 until totalreagents.size) {
                                //if the first member of the pair is the drug
                                //it adds its dosage to the second so it can be sumed later
                                if (totalreagents[druglist].first == drug.drugName) {
                                    totalreagents[druglist].second.add(drug.dosage)
                                } else {
                                    totalreagents.add(Pair(drug.drugName, arrayOf<Float>().toMutableList()))
                                }
                            }
                        }
                        //this is the part where the drugs are done.
                        for (dodrugs in totalreagents){
                            drugswitch(Drugs.DrugEdible(dodrugs.first, dodrugs.second.sum()), player)
                        }
                        //this does drug decay and removal
                        if (drug.dosage / 2 <= 0.1) {
                            reagents.removeAt(drugit)
                            player.overRideReagents(reagents)
                            continue
                        }
                        drug.halfLifeTick(everytick)
                        println(drug.drugName.toString() + ", " + drug.dosage.toString())
                    }
                }
                catch(e: Exception){ println("OOPS") }
            }
            //sets the interval inwhich this check happens.
            //20 ticks in a second
            //subtract 1 to have it work evenly.
            counter+=everytick-1
        }
        else{ counter+=-1 }
    })
}

//a test -- exampel for callbacks

class Working : AttackBlockCallback{
     override fun interact(
        player: PlayerEntity,
        world: World,
        hand: Hand,
        pos: BlockPos,
        direction: Direction): ActionResult{

         if (player.canTakeDamage()){
             println(player.getdamage(NewDamage.total))

             player.changedamage(NewDamage.burn, -10F)
        return ActionResult.PASS }
    println("test")
    return ActionResult.PASS }
}



//Contains, mounnting the various functions relating to damage
fun LivingEntity.getdamage(types : NewDamage) : Float{
    return Containers.getdamage(this, types)
}
fun LivingEntity.setdamage(types : NewDamage, input: Float) : Float{
    return Containers.setdamage(this, types, input)
}
fun LivingEntity.changedamage(types : NewDamage, input: Float) : Boolean {
    return Containers.changedamage(this, types, input)
}
fun PlayerEntity.removeReagent(drug : Drugs.DrugEdible){
    Containers.removeReagent(this, drug)
}
fun PlayerEntity.listReagents() : MutableList<Drugs.DrugEdible>{
    return Containers.listReagents(this)
}
fun PlayerEntity.overRideReagents(array: MutableList<Drugs.DrugEdible>){
    Containers.overRideReagents(this, array.toMutableList())
}
fun playerinlist(player: ServerPlayerEntity,
                 list : MutableList<Pair<ServerPlayerEntity, MutableList<Drugs.DrugEdible>>>) : Boolean{
    for (i in list){
        if (i.first == player){ return true }}
    return false}





