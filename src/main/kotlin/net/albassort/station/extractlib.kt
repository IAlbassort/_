package net.albassort.station

import java.io.*
import java.io.File
import java.lang.Thread
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
//this was a way to extract the libraries included in the archive for execution
class libinit {
    companion object {
        fun gettempdir(): File {
            val paths: String =
                    libinit::class.java.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()
            val actualdir: String = getdir(paths)
            var tempdir: File = File(actualdir + "templib")
            return tempdir
        }

        @JvmStatic
        fun extractlibraries() {
            // makes new dir lib
            // USEFUL SHIT HERE, PLEASE REMEMBER CAROLINE YOU CAN DO THIS!!!!
            Runtime.getRuntime().addShutdownHook(object : Thread() {
                override fun run() {
                    removetemp()}
                })
            val tempdir: File = gettempdir()
            if (!tempdir.exists()) {
                tempdir.mkdir()
            } else {
                removetemp()
                println("we should be removing temp...")
                tempdir.mkdir()
            }
            //ugly code but those v vars don't need to be in memory.
            val lookup: String = "${System.getProperty("os.name").lowercase()}_${System.getProperty("os.arch").lowercase()}"
            val input: InputStream = libinit::class.java.getResourceAsStream("/nimmeta.txt")
            val libtext: List<String> =
                    String(input.readAllBytes(), StandardCharsets.UTF_8).split("\n")
            // gets os and reads for the list of supported OS's
            if (!(lookup in libtext)) {
                throw Exception("OS NOT SUPPORTED")
            }
            var libslist: MutableList<String> = ArrayList()
            // makes mutablelist to extract stuff from the loop because local variables....
            for (lib in libtext) {
                if (lib.isBlank()){
                    continue
                }
                if (lib.toCharArray()[0] == "#".toCharArray()[0]) {
                    var libcheck = lib.split("#")[1]
                    libslist.add(libmodify(libcheck))
                    }}
                    // uses cool when statement to modify the vars on the case for the different
                    // supported oses.
                
            for (lib in libslist) {
                println(lookup)
                println(lib)
                val testlib = libinit::class.java.getResourceAsStream("/natives/${lookup}/${lib}")
                Files.copy(testlib, Paths.get(tempdir.toString() + "/${lib}"))
            }
        }
    }
}


fun getdir(input: String): String {
    return input.split(input.split("/").last())[0]
}

fun removetemp() {
    for (i in gettempdir().list()) {
        val newfile: File = File("${gettempdir()}" + "/${i}")
        newfile.delete()
    }
    gettempdir().delete()
}
fun gettempdir() : File{
    val paths: String = libinit::class.java.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()
    val actualdir: String = getdir(paths)
    var tempdir: File = File(actualdir + "templib")
    return tempdir
}
fun libmodify(libcheck : String) : String{
    val output : MutableList<String> = ArrayList()
    when (System.getProperty("os.name").lowercase()){
        "windows" -> output.add(libcheck + ".dll")
        "linux" -> output.add("lib" + libcheck + ".so")}
    return output[0]
}