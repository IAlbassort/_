import net
import strutils
import math
import os


proc sendtojava(s: string) {.cdecl, exportc, dynlib, thread.} = 
    var sendtojava = newSocket()
    sendtojava.connect("127.0.0.1", Port(2021))
    sendtojava.send(s & "\r\L")

proc priv_prime(startint : int, endint: int) : seq[int] {.cdecl, exportc, dynlib.} = 
  var primenumbers: seq[int]
  for i in startint .. endint:
    for x in 1..i+1:
      var check = i/x
      var wholenumber = check - check.floor()
      if (wholenumber) == 0.0:
        if x == i:
          primenumbers.add(i)
        if x != 1:
          break
  return primenumbers

proc print(s : string) =
  echo "Nim: "&s

proc helloworld() {.cdecl, exportc, dynlib, thread.} =
  var client: Socket
  var address = "127.0.0.1"
  var socket = newSocket()
  let input : string = "test.1.100.uuid1982"
  var container: seq[string]
  os.sleep(10000)
  socket.bindAddr(Port(2022))
  socket.listen()
  print "Started!"
  while true:
    socket.acceptAddr(client, address)  
    print "connected"

    while true:
      let message: string = client.recvLine()
      print message
      if message[0..3] == "test":
          print "doing!"
          let inseq = split(input)
          if container.contains(inseq[3]) == false:
              container.add(inseq[3])
          print "done!"
          sendtojava("test." & $(priv_prime(parseInt(inseq[1]), parseInt(inseq[2])*1000)))
      if message == "quit":
        print "test"
        quit("sent exit command!")
      if message == "":
        break
      echo "not doing"



